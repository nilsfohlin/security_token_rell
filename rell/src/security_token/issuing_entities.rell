
// The company/fund/person who has assets
// Belongs to platform
entity issuing_entity {
	key name;
	platform: platform;
	mutable operable: boolean;
	mutable suspended: boolean;
	mutable meta: json;
	mutable open_to_level_above: boolean;
}

// issuing_company_representative 
// Belong to issuing_company
entity ie_rep {
	key account: ft3.acc.account;
	issuing_entity: issuing_entity;
	mutable operable: boolean;
	mutable suspended: boolean;
	rep_title;
}

enum rep_title {
	SuperManager,
	Manager,
	Editor
}

/*
 * OPERATIONS
 */

operation add_issuing_entity (
	_name: name,
	_meta: json,
	_admin: platform_admin
){
	_check_platform_admin(_admin);
	require(_name != "", "Platform must have a name");
	
	create issuing_entity (_name, _admin.platform, _meta, operable = false, suspended = false, open_to_level_above = false);
}

operation issuing_entity_operable (
	_issuing_entity: issuing_entity,
	_operable: boolean,
	_admin: platform_admin
){
	_check_platform_admin(_admin);
	_check_valid_platform(_admin, _issuing_entity);
	
	if (_issuing_entity.operable != _operable){
		_issuing_entity.operable = _operable;
	}
}

operation issuing_entity_suspended(
	_issuing_entity: issuing_entity,
	_suspended: boolean,
	_admin: platform_admin 
){
	_check_platform_admin(_admin);
	_check_valid_platform(_admin, _issuing_entity);
	
	if (_issuing_entity.suspended != _suspended){
		_issuing_entity.suspended = _suspended;
	}
}

operation admin_add_ie_rep(
	_rep_acc_id: byte_array,
	_issuing_entity: issuing_entity,
	_rep_title: text,
	_admin: platform_admin 
){
	_check_platform_admin(_admin);
	_check_valid_platform(_admin, _issuing_entity);
	val rep_acc = require(ft3.acc.account @? {_rep_acc_id}, "Not a registered ft3 account");
	require(_issuing_entity.suspended == false, "Issuing entity is suspended");
	require(empty(investor @? {.account.id == _rep_acc_id}), "User cannot be registered as other platform user");
	
	create ie_rep(rep_acc, _issuing_entity, operable = true, suspended = false, rep_title.value(_rep_title));
}

operation admin_ie_rep_operable(
	_ie_rep: ie_rep,
	_operable: boolean,
	_admin: platform_admin
){
	_check_platform_admin(_admin);
	_check_valid_platform(_admin, _ie_rep.issuing_entity);
	
	if (_ie_rep.operable != _operable){
		_ie_rep.operable = _operable;
	}
}

operation admin_ie_rep_suspended(
	_ie_rep: ie_rep,
	_suspended: boolean,
	_admin: platform_admin
){
	_check_platform_admin(_admin);
	_check_valid_platform(_admin, _ie_rep.issuing_entity);
	
	if (_ie_rep.suspended != _suspended){
		_ie_rep.suspended = _suspended;
	}
}

operation manager_add_ie_rep(
	_rep_acc_id: byte_array,
	_rep_title: text,
	_super_manager: ie_rep, 
	_sm_auth_id: byte_array
){
	require(_check_log_ie_rep(_super_manager, _sm_auth_id, list<text>()) == rep_title.value("SuperManager"), "Only SuperManager can add new ie_rep");
	val rep_acc = require(ft3.acc.account @? {_rep_acc_id}, "Not a registered ft3 account");
	require(empty(investor @? {.account.id == _rep_acc_id}), "User cannot be registered as other platform user");
	
	create ie_rep(rep_acc, _super_manager.issuing_entity, operable = true, suspended = false, rep_title.value(_rep_title));
}

operation manager_ie_rep_operable(
	_ie_rep: ie_rep,
	_operable: boolean,
	_super_manager: ie_rep, 
	_sm_auth_id: byte_array
){
	require(_check_log_ie_rep(_super_manager, _sm_auth_id, list<text>()) == rep_title.value("SuperManager"), "Only SuperManager can toggle ie_rep operable");
	require(_ie_rep.issuing_entity == _super_manager.issuing_entity, "Super manager can only control managers or editors within the same issuing entity");
	
	if (_ie_rep.operable != _operable){
		_ie_rep.operable = _operable;
	}
}

/*
 * FUNCTIONS
 */

function _check_log_ie_rep(_ie_rep: ie_rep, _auth_descriptor_id: byte_array, _list: list<text>){
	ft3.acc.auth_and_log(_ie_rep.account.id, _auth_descriptor_id, list<text>());
	return _check_ie_rep(_ie_rep);
}


function _check_valid_platform(_admin: platform_admin, _issuing_entity: issuing_entity){
	require(_admin.platform == _issuing_entity.platform, "Invalid admin for this platform");
}

function _check_ie_rep(_ie_rep: ie_rep){
	require(_ie_rep.operable == true, "operable == false");
	require(_ie_rep.suspended == false, "suspended == true");
	return _ie_rep.rep_title;
}

function _check_ie(_issuing_entity: issuing_entity){
	require(_issuing_entity.operable == true, "issuing_entity operable == false");
	require(_issuing_entity.suspended == false, "issuing_entity suspended == true");
	require(_issuing_entity.platform.operable == true, "platform operable == false");
}

/*
 * QUERIES
 */

query get_ie_id_by_name(_name: text){
	return issuing_entity @? {.name == _name};
}

query get_issuing_entity_data(_issuing_entity: issuing_entity){
	return (
		name = _issuing_entity.name,
		platform_id = _issuing_entity.platform,
		operable = _issuing_entity.operable,
		suspended = _issuing_entity.suspended,
		meta = _issuing_entity.meta,
		open_to_level_above = _issuing_entity.open_to_level_above
	);
}

query get_ie_rep(_acc_id: byte_array){
	return ie_rep @? {.account.id == _acc_id};
}

query get_ie_rep_data(_acc_id: byte_array){
	return ie_rep @? {.account.id == _acc_id}(
		id = .account.id,
		issuing_entity = .issuing_entity,
		platform = .issuing_entity.platform,
		operable = .operable,
		suspended = .suspended,
		title = .rep_title
	);
}
